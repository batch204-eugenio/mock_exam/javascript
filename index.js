function countLetter(letter, sentence) {
  let result = 0;

    if (letter.length == 1) {

        for (let i = 0; i < sentence.length; i++) {

            if(letter == sentence[i]) {
                result++;
            }
        }
    } else {
        return undefined;
    }
    
    return result;


    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    //==count how many time the letters appear in a sentence or word. (a, banana) ilang beses lumabas si a.
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let result = true;
    text = text.toLowerCase();
    for (let a = 0; a < text.length; a++) {

        for (let b = a+1; b < text.length; b++) {
            if(text[a] == text[b]) {
                return false;
            }
        } 
    }  
    return result;
}

function purchase(age, price) {
    if(age < 13 ) {
        return undefined;
    } else if(age >= 22 && age <= 64) {
        let roundedOffPrice = (Math.round(price * 100) / 100)
        return String(roundedOffPrice)
    } else {
        let discount = price - (price * .20);
        let discountedPrice = Math.round(discount * 100) / 100;
        return String(discountedPrice)
    }
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

  let categories = [];
  for(i = 0; i <=items.length; i++)
    if(categories[i].index) {
        continue;
    }
    categories.push(items[i].index)

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
  
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
 
    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
  
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};















